<?php 
    require_once '../bootstrap/bootstrap_application.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>PHP WAR</title>
    <meta name="author" content="Ivan Djukic">
    <link rel="stylesheet" 
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" 
        crossorigin="anonymous">
    <link rel="stylesheet" 
        href="./Assets/style.css"> 
</head>
<body class="text-center">
    <?php 
        $army1_size = $_GET['army1'];
        $army2_size = $_GET['army2'];
    ?>
    <?php if(!isset($army1_size) || !isset($army1_size) || !is_numeric($army1_size) || !is_numeric($army2_size)): ?>
        <form class="chose-number-of-soldiers" action="/" method="GET">
            <h1 class="h3 mb-3 font-weight-normal">Please insert number of soldiers in both armies</h1>
            
            <label for="army1" class="sr-only">Army 1</label>
            <input 
                type="number" 
                id="army1" 
                class="form-control" 
                name="army1" 
                placeholder="Army 1"
                min="10" 
                max="1000" 
                required="" 
                autofocus="">
            
            <label for="army2" class="sr-only">Army 2</label>
            <input 
                type="number" 
                id="army2" 
                class="form-control" 
                name="army2" 
                placeholder="Army 2" 
                min="10" 
                max="1000" 
                required=""
                autofocus="">
            
            <button class="btn btn-lg btn-primary btn-block" type="submit">Start fight</button>
        </form>
    <?php else: ?> 
        <?php 
            {
                $army1_size = intval($army1_size);
                $army2_size = intval($army2_size);

                $new_game = new \Classes\Game('first_army', $army1_size, 'seccond_army', $army2_size);
                $new_game->startGame();
            }
        ?>
        <div class="start-new-game">
            <p>
                <?php echo \Services\LogService::getMatchLog();?>
            </p>
            <a href="/" class="btn btn-primary"> New Game </a>
        </div>
        
    <?php endif; ?>
</body>
</html>