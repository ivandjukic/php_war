<?php 
    
namespace Classes;

use \Services\RandomNumberGeneratorService as RandomNumberGeneratorService;
use \Models\GeneralModel as GeneralModel;
use \Models\TacticModel as TacticModel;
use \Classes\Session as Session;

class General {
    
    public static function getRandomGeneral() {
        $general = null;
        $already_selected_general = Session::get('army_one_general');
        while(!$general) {
            $random_general = GeneralModel::inRandomOrder()->first();
            if($random_general && $random_general->id != $already_selected_general->id) {
                $general = $random_general;
            }
        }
        return $general;
    }   

    public static function choseTactic() {
        $random_tactict = TacticModel::inRandomOrder()->first();
        return $random_tactict;
    }

    public static function getGeneralSkills($general, $tactic) {

    }
}