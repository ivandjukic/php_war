<?php

namespace Classes;

use \Classes\Session as Session;
use \Classes\General as General;
use \Models\GeneralSkillModel as GeneralSkillModel;
use \Models\TacticEfficientModel as TacticEfficientModel;
use \Models\MilitaryUnitModel as MilitaryUnitModel;
use \Services\LogService as LogService;

class Army {
    
    public $army_name = null;
    public $number_of_archers = 0;
    public $number_of_horsmens = 0;
    public $number_of_knights = 0;
    public $motivation = 0;
    public $total_attack_power = 0;
    public $total_defense_power = 0;
    
    
    public function __construct($number_of_soldiers, $army_name)
	{
        $number_of_archers = rand(0, $number_of_soldiers);
		$number_of_horsmens = rand(0, $number_of_soldiers - $number_of_archers);
        $number_of_knights = $number_of_soldiers - ($number_of_archers + $number_of_knights);
        
        $this->number_of_archers = $number_of_archers;
        $this->number_of_horsmens = $number_of_horsmens;
        $this->number_of_knights = $number_of_knights;
        $this->army_name = $army_name;

    }

    public function calculateMotivation($general, $tactic, $another_army_tactic) {
        $general_skills = GeneralSkillModel::query()
            ->where('general_id', $general->id)
            ->where('tactic_id', $tactic->id)
            ->first();
        $tactics_efficient = TacticEfficientModel::query()
            ->where('first_tactic_id', $tactic->id)
            ->where('second_tactic_id', $another_army_tactic->id)
            ->first();
        
        $this->motivation = $general_skills->skill_points + $tactics_efficient->points;
    }

    public function calculateTotalAttackPower() {
        $total_attack_power = 0;
        $total_attack_power += $this->number_of_archers * $this->getAttackPower('archers');
        $total_attack_power += $this->number_of_horsmens * $this->getAttackPower('horsmens');
        $total_attack_power += $this->number_of_knights * $this->getAttackPower('knights');
        $this->total_attack_power = $total_attack_power;
    }

    public function calculateTotalDefensePower() {
        $total_defense_power = 0;
        $total_defense_power += $this->number_of_archers * $this->getDefensePower('archers');
        $total_defense_power += $this->number_of_horsmens * $this->getDefensePower('horsmens');
        $total_defense_power += $this->number_of_knights * $this->getDefensePower('knights');
        $this->total_defense_power = $total_defense_power;
    }

    private function getAttackPower($army_name) {
        $army_type  = MilitaryUnitModel::query() 
            ->where('name', $army_name)
            ->first();
        return $army_type->attack;
    }

    private function getDefensePower($army_name) {
        $army_type  = MilitaryUnitModel::query() 
            ->where('name', $army_name)
            ->first();
        return $army_type->defense;
    }

    public function attack($seccond_army) {
        $attack_power = $this->calculateAttack();
        $seccond_army->total_defense_power -= $attack_power;
        if($seccond_army->total_defense_power < 0) {
            $seccond_army->total_defense_power = 0;
            $seccond_army->number_of_archers = 0;
            $seccond_army->number_of_horsmens = 0;
            $seccond_army->number_of_knights = 0;
        }
        LogService::log("<p>$this->army_name attacked $seccond_army->army_name, damage: <b>$attack_power</b>, 
            $seccond_army->army_name defense points left: <b>$seccond_army->total_defense_power</b></p>");
    }

    private function calculateAttack() {
        return round($this->total_attack_power + ($this->total_attack_power * ($this->motivation / 100)));
    }



}