<?php 
    
namespace Classes;

class Session {
    public static function initSession() {
        session_start();
        session_unset();
    }

    public static function put($key, $value) {
        try {
            $_SESSION[$key] = $value;
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    public static function get($key) {
        return $_SESSION[$key];
    }
    
}