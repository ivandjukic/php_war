<?php

namespace Classes;

use \Classes\General as General;
use \Classes\Session as Session;
use \Classes\Army as Army;
use \Services\LogService as LogService;

class Game {
    
    private $army_one_size;
    private $army_one_name;
    private $army_two_size;
    private $army_two_name;
    private $winner;
	
	public function __construct($army_one_name, $army_one_size, $army_two_name, $army_two_size)
	{
        $this->army_one_size = $army_one_size;
        $this->army_one_name = $army_one_name;
        LogService::log("<p>Army one created, number of soldiers: <b> $army_one_size </b>, army name: $army_one_name</p>");
        $this->army_two_size = $army_two_size;
        $this->army_two_name = $army_two_name;
        LogService::log("<p>Army two created, number of soldiers: <b>$army_two_size</b>, army name: $army_two_name</p>");
    }

    public function startGame() {
        $army_one_general = General::getRandomGeneral();
        LogService::log("<p>Army one general created. General name: <b>$army_one_general->name</b></p>");
        Session::put('army_one_general', $army_one_general);
        $army_two_general = General::getRandomGeneral();
        Session::put('army_two_general', $army_one_general);
        LogService::log("<p>Army two general created. General name: <b>$army_two_general->name</b></p>");

    
        $army_one_tactic = General::choseTactic();
        LogService::log("<p>Army one tactic selected. Tactic name: <b>$army_one_tactic->name</b></p>");
        $army_two_tactic = General::choseTactic();
        LogService::log("<p>Army one tactic selected. Tactic name: <b>$army_two_tactic->name</b></p>");

        $army_one = new Army($this->army_one_size, $this->army_one_name);
        $army_one->calculateMotivation($army_one_general, $army_one_tactic, $army_two_tactic);
        $army_one->calculateTotalAttackPower();
        $army_one->calculateTotalDefensePower();
        LogService::log("<p>Army one structure:<b>" . json_encode($army_one) . "</b></br>");

        $army_two = new Army($this->army_two_size, $this->army_two_name);
        $army_two->calculateMotivation($army_two_general, $army_two_tactic, $army_one_tactic);
        $army_two->calculateTotalAttackPower();
        $army_two->calculateTotalDefensePower();
        LogService::log("<p>Army two structure:<b>" . json_encode($army_two) . "</b></br>");

        $war = true;
        while($war) {
            if($army_one->total_defense_power > 0 && $army_two->total_defense_power > 0) {
                $army_one->attack($army_two);
            } else {
                LogService::log("<p><b>Army one won this game</b></br>");
                break;
            }
            if($army_one->total_defense_power > 0 && $army_two->total_defense_power > 0) {
                $army_two->attack($army_one);
            } else {
                LogService::log("<p><b>Army two won this game</b></br>");
                break;
            }
        }

    }    
}