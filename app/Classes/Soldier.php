<?php

abstract class Soldier {
    
    private $defense = 0;
    private $attack = 0;

    public function __construct($defense, $attack)
    {
        $this->defense = $defense;
        $this->attack = $attack;
    }

    
}