<?php

namespace Services;

use \Classes\Session as Session;

class LogService {

    public function log($message) {
        if(!Session::get('match_details')) {
            Session::put('match_details', "$message\n");
        } else {
            $new_message = Session::get('match_details') . "<br> $message";
            Session::put('match_details', $new_message);
        }
    }
    
    public function getMatchLog() {
        return Session::get('match_details');
    } 

}
