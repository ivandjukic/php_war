<?php
    require_once('Helpers/file_system.php');
    require base_path('vendor/autoload.php');
    require base_path('app/Classes/Game.php');
    require base_path('app/Classes/General.php');

    //Initialize Illuminate Database Connection
    use Models\Database;
    new Database();

    //Start Session
    \Classes\Session::initSession();
    

    
?>