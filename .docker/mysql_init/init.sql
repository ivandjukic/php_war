-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: degordian_test
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `general_skills`
--

DROP TABLE IF EXISTS `general_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `general_skills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `general_id` int(11) NOT NULL,
  `tactic_id` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `general_skills`
--

LOCK TABLES `general_skills` WRITE;
/*!40000 ALTER TABLE `general_skills` DISABLE KEYS */;
INSERT INTO `general_skills` VALUES (1,1,1,34),(2,1,2,32),(3,1,3,47),(4,1,4,10),(5,1,5,20),(6,2,1,19),(7,2,2,27),(8,2,3,28),(9,2,4,10),(10,2,5,36),(11,3,1,20),(12,3,2,23),(13,3,3,45),(14,3,4,29),(15,3,5,38),(16,4,1,16),(17,4,2,35),(18,4,3,36),(19,4,4,26),(20,4,5,14),(21,5,1,24),(22,5,2,26),(23,5,3,11),(24,5,4,45),(25,5,5,22),(26,6,1,48),(27,6,2,42),(28,6,3,18),(29,6,4,34),(30,6,5,26),(31,7,1,17),(32,7,2,40),(33,7,3,20),(34,7,4,49),(35,7,5,15),(36,8,1,38),(37,8,2,15),(38,8,3,30),(39,8,4,19),(40,8,5,35),(41,9,1,27),(42,9,2,23),(43,9,3,23),(44,9,4,35),(45,9,5,16),(46,10,1,47),(47,10,2,19),(48,10,3,24),(49,10,4,12),(50,10,5,21);
/*!40000 ALTER TABLE `general_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generals`
--

DROP TABLE IF EXISTS `generals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generals` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generals`
--

LOCK TABLES `generals` WRITE;
/*!40000 ALTER TABLE `generals` DISABLE KEYS */;
INSERT INTO `generals` VALUES (2,'Alaric the Visigoth'),(1,'Alexander the Great'),(3,'Attila the Hun'),(4,'Cyrus the Great'),(5,'Hannibal'),(6,'Julius Caesar'),(9,'Marius'),(7,'Scipio Africanus'),(8,'Sun Tzu'),(10,'Trajan');
/*!40000 ALTER TABLE `generals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `military_units`
--

DROP TABLE IF EXISTS `military_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `military_units` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `defense` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `defense` (`defense`),
  UNIQUE KEY `attack` (`attack`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `military_units`
--

LOCK TABLES `military_units` WRITE;
/*!40000 ALTER TABLE `military_units` DISABLE KEYS */;
INSERT INTO `military_units` VALUES (1,'archers',20,8),(2,'horsmens',6,7),(3,'knights',40,5);
/*!40000 ALTER TABLE `military_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tactics`
--

DROP TABLE IF EXISTS `tactics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tactics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tactics`
--

LOCK TABLES `tactics` WRITE;
/*!40000 ALTER TABLE `tactics` DISABLE KEYS */;
INSERT INTO `tactics` VALUES (5,'Ambush'),(4,'Force concentration'),(3,'Guerrilla'),(1,'Marching fire'),(2,'Night combat ');
/*!40000 ALTER TABLE `tactics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tactics_efficient`
--

DROP TABLE IF EXISTS `tactics_efficient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tactics_efficient` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_tactic_id` int(11) NOT NULL,
  `second_tactic_id` int(11) NOT NULL,
  `points` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tactics_efficient`
--

LOCK TABLES `tactics_efficient` WRITE;
/*!40000 ALTER TABLE `tactics_efficient` DISABLE KEYS */;
INSERT INTO `tactics_efficient` VALUES (1,1,1,19),(2,1,2,21),(3,1,3,38),(4,1,4,40),(5,1,5,37),(6,2,1,13),(7,2,2,24),(8,2,3,34),(9,2,4,46),(10,2,5,39),(11,3,1,48),(12,3,2,35),(13,3,3,19),(14,3,4,21),(15,3,5,40),(16,4,1,48),(17,4,2,29),(18,4,3,33),(19,4,4,27),(20,4,5,28),(21,5,1,47),(22,5,2,24),(23,5,3,47),(24,5,4,34),(25,5,5,19);
/*!40000 ALTER TABLE `tactics_efficient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'degordian_test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-07 19:59:00